const { authenticate } = require('@feathersjs/authentication').hooks;
// const queryMenu = require('./../../hooks/authorize');
const { when } = require('feathers-hooks-common');
const authorize = require('./../../hooks/abilities');

module.exports = {
  before: {
    all: [
      authenticate('jwt'),
      when(
        hook => hook.params.provider && `/${hook.path}` !== hook.app.get('authentication').path,
        authorize()
      ),
      // queryMenu
    ],
    find: [
      // async context => {
      //   const sequelize = context.app.get('sequelizeClient');
      //
      //   const { permissions } = sequelize.models;
      //   context.params.sequelize = {
      //     include: [ { model: permissions, attributes: ['id', 'name', 'slug', 'menu_id', 'status', 'view', 'create', 'update', 'delete', 'export', 'all'], as: 'permissions' } ]
      //   }
      // }
    ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
