// Initializes the `residential_complexes` service on path `/residential_complexes`
const { residentialComplexes } = require('./residential_complexes.class');
const createModel = require('../../models/residential_complexes.model');
const hooks = require('./residential_complexes.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app)
    // paginate: false// app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/residential_complexes', new residentialComplexes(options, app), async function(req, res, next) {
    const result = res.data;
    if ((req.method === 'POST' || req.method === 'post') && !result.id) {
      let newData = await app.service('residential_complexes').find({
        query: {
          name: result.name,
          createdBy: result.createdBy,
          createdAt: result.createdAt,
        }
      });
      res.data.id = newData[0].id;
    }
    next();
  });

  // app.use('/residential_complexes/:id', new residentialComplexes(options, app), async function(req, res, next) {
  //   const result = res.data;
  //   const data = result.data; // will be either `result` as an array or `data` if it is paginated
  //   next();
  // });

  // app.use('/residential_complexes/:id/flats', new residentialComplexes(options, app), async function(req, res, next) {
  //   const result = res.data;
  //   const data = result.data; // will be either `result` as an array or `data` if it is paginated
  //   next();
  // });

  // Get our initialized service so that we can register hooks
  const service = app.service('residential_complexes');

  service.hooks(hooks);
  service.publish((data, hook) => {
    return app.channel(`connections/${data.to}`)
  })
};
