const { Service } = require('feathers-sequelize');

exports.UserRoles = class UserRoles extends Service {
  create(data, params) {
    return super.create(data, params);
  }
  find(params) {
    return super.find(params);
  }
  get(id, params) {
    return super.get(id, params);
  }
};
