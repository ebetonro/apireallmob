// Initializes the `contacts` service on path `/contacts`
const { Contacts } = require('./contacts.class');
const createModel = require('../../models/contacts.model');
const hooks = require('./contacts.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/contacts', new Contacts (options, app), async function(req, res, next) {
    const result = res.data;
    const data = result.data; // will be either `result` as an array or `data` if it is paginated
    console.log(data); // when `data` will be used, this should disapear
    next();
  });

  app.use('/contacts/:id', new Contacts (options, app), async function(req, res, next) {
    const result = res.data;
    const data = result.data; // will be either `result` as an array or `data` if it is paginated
    console.log(data); // when `data` will be used, this should disapear
    next();
  });

  // Get our initialized service so that we can register hooks
  const service = app.service('contacts');

  service.hooks(hooks);
};
