const { Service } = require('feathers-sequelize');

exports.Permissions = class Permissions extends Service {
  find(params) {
    params.sequelize = {
      distinct: true,
      group: ['role_id', 'name']
    };
    return super.find(params);
  }
  get(id, params) { return super.get(id, params) }
  remove(id, params) { return super.remove(id, params) }
  create(data, params) {
    if (!data.createdBy) {
      data.createdBy = params.user.id
    }
    return super.create(data, params)
  }

  update(id, data, params) {
    if (!data.updatedBy) {
      data.updatedBy = params.user.id
    }
    return super.update(id, data, params)
  }
  patch(id, data, params) {
    if (!data.updatedBy) {
      data.updatedBy = params.user.id
    }
    return super.patch(id, data, params)
  }
};
