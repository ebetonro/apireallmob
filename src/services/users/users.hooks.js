const { authenticate } = require('@feathersjs/authentication').hooks;
const verifyHooks = require('feathers-authentication-management').hooks;
const addAssociations = require('./../../hooks/add-associations');
// const getRelated = require('./../../hooks/get-related');
// const authorize = require('./../../hooks/authorize');
const accountService = require('../authmanagement/notifier');
const commonHooks  = require('feathers-hooks-common');
const validator = require('validator');

const { when } = require('feathers-hooks-common');
const authorize = require('./../../hooks/abilities');

const {
  hashPassword, protect
} = require('@feathersjs/authentication-local').hooks;

module.exports = {
  before: {
    all: [],
    find: [
      authenticate('jwt'),
      when(
          hook => hook.params.provider && `/${hook.path}` !== hook.app.get('authentication').path,
        authorize(),
      ),
    ],
    get: [
      authenticate('jwt'),
      when(
        hook => hook.params.provider && `/${hook.path}` !== hook.app.get('authentication').path,
        authorize(),
      ),
    ],
    create: [
      hashPassword('password'),
      async context => {
        let input = context.data;
        //const userService = app.service('users');

        if (validator.isEmpty(input.name)) {
          throw new Error('Please enter your name.');
        }
        if (validator.isEmpty(input.email)) {
          throw new Error('Please enter your email address.');
        } else if (!validator.isEmail(input.email)) {
          throw new Error('Please enter a valid email address.');
        }
        if (validator.isEmpty(input.password)) {
          throw new Error('Please enter a password for your account.');
        }

        if (context.data.email_changed) {
          context.data.email_changed = 0
        }
        if (context.data.email_validated) {
          context.data.email_validated = 0
        }

        await context.service.find({
          query: {
            email: input.email
          }
        }).then((data) => {
          if (data.data.length) {
            throw new Error('This email address is already in use by somebody else.');
          }
        });
      },
      verifyHooks.addVerification()
      ],
    update: [ hashPassword('password'),  authenticate('jwt') ],
    patch: [ hashPassword('password'),  authenticate('jwt') ],
    remove: [ authenticate('jwt') ]
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password')
    ],
    find: [],
    get: [
      addAssociations()
    ],
    create: [
      context => {
        accountService(context.app).notifier('resendVerifySignup', context.result)
      },
      verifyHooks.removeVerification()
    ],
    update: [
      commonHooks.disallow('external')
    ],
    patch: [
      commonHooks.iff(
        commonHooks.isProvider('external'),
        commonHooks.preventChanges(true,
          'email',
          'isVerified',
          'verifyToken',
          'verifyShortToken',
          'verifyExpires',
          'verifyChanges',
          'resetToken',
          'resetShortToken',
          'resetExpires'
        ),
        hashPassword('password'),
        authenticate('jwt')
      )
    ],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
