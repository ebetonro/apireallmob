const { Users } = require('./users.class');

const createModel = require('../../models/users.model');
const hooks = require('./users.hooks');
const { authenticate } = require('@feathersjs/express');
const { Forbidden } = require('feathers-errors');
const { Upload } = require('../../upload.class');
var path = require('path');
const fs = require('fs');
const im = require('imagemagick');

// Add more from http://en.wikipedia.org/wiki/List_of_file_signatures
const mimeType = function (headerString) {
  switch (headerString) {
    case "89504e47":
      type = "image/png";
      break;
    case "47494638":
      type = "image/gif";
      break;
    case "ffd8ffe0":
      type = false;
      break;
    case "ffd8ffe1":
      type = false;
      break;
    case "ffd8ffe2":
      type = "image/jpeg";
      break;
    default:
      type = false;
      break;
  }
  return type;
};

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/users', new Users(options, app), async function(req, res, next) {
    const result = res.data;
    if ((req.method === 'POST' || req.method === 'post') && !result.id) {
      let newData = await app.service('users').find({
        query: {
          name: result.name,
          createdBy: result.createdBy,
          createdAt: result.createdAt,
        }
      });
      res.data.id = newData[0].id;
    }
    next();
  });
  const todoService = {
    async get(id, params) {
      console.log(params);
      return this.app.service('user-roles').get(id, params)
    },

    async find(params) {
      let result = await this.app.service('user-roles').find({
        query: Object.assign({
          id: params.route.id,
          user_id: params.route.user_id
        }, params.query)
      });

      let role;
      if (result && result.data && result.data.length > 0) {
        const serviceRole = this.app.service('roles');
        let dataResult = result.data
        if (result.total > 1) {
          console.log(result);
          for (let indexComplexRole in dataResult) {
            role[dataResult[indexComplexRole].residential_complex_id] = {};
            role[dataResult[indexComplexRole].residential_complex_id] = await serviceRole.find({
              query: Object.assign({
                id: dataResult[indexComplexRole].role_id
              }, params.query)
            })
          }
        } else {
          role[dataResult[0].residential_complex_id] = await serviceRole.find({
            query: Object.assign({
              id: dataResult[0].role_id
            }, params.query)
          })
        }
      }

      return result;
    },

    setup(app) {
      this.app = app;
    }

  };

  // app.get('/users/:user_id/user-roles/:id/roles', authenticate('jwt') //, todoService, function (req, res) {
  //   // console.log(res.data)
  // // }
  //   , function(req, res) {
  //   console.info('...................................')
  //     var itemService = app.service('user-roles');
  //     var user_id = req.params.user_id;
  //     var itemIndex = req.params.id;
  //
  //     itemService.find({ query: {
  //         user_id: user_id
  //       }
  //     }, function(error, items) {
  //       console.log(items)
  //       res.json(items[itemIndex]);
  //     });
  //   }
  // );

  app.use('/users/:user_id/uploads', authenticate('jwt'), async function (req, res) {
    const { user } = req;
    var user_id = req.params.user_id;

    console.log('Request:');

    if (parseInt(user.id) !== parseInt(user_id)) {
      throw new Forbidden(`You are not allowed to do this action!`);
    }
    // let appDir = path.dirname(require.main.filename);
    // let dir = appDir + '/uploads';
    let classUpload = new Upload(req, res, app, 'image', 'file', 'avatar', 1, 20);
    await classUpload.uploadStart();

  });
  // {
  //   async get (id, params) {
  //     console.log(params.query); // -> ?query
  //     console.log(params.provider); // -> 'rest'
  //     console.log(params.fromMiddleware); // -> 'Hello world'
  //     console.log(params.route.userId);
  //     const { user } = params;
  //     console.log('asfasda', user);
  //     return {
  //       id,
  //       params,
  //       read: false,
  //       text: `Feathers is great!`,
  //       createdAt: new Date().getTime()
  //     }
  //   },
  //   async create (data, params) {
  //     console.log(params.query); // -> ?query
  //     console.log(params.provider); // -> 'rest'
  //     console.log(params.fromMiddleware); // -> 'Hello world'
  //     console.log(params.route.userId);
  //     const { user } = params;
  //     console.log('asfasda', params);
  //     return {
  //       data,
  //       params
  //     }
  //   }
  //}

  app.use('/users/:user_id/image/:name', authenticate('jwt'), async function (req, res) {
    const { user, method, params } = req;

    var user_id = req.params.user_id;
    var name = req.params.name;

    if (parseInt(user.id) !== parseInt(user_id)) {
      throw new Forbidden(`You are not allowed to do this action!`);
    } else if (!name) {
      throw new Forbidden(`You are not allowed to do this action with out a name!`);
    }

    let dir = path.dirname(require.main.filename) + '/uploads';
    var width = req.query.w ? req.query.w : false;
    var height = req.query.h ? req.query.h : false;

    let folderResized = (width && height ? '/images/resized/' + width + 'x' + height : '/images');
    let encoding = 'base64';

    if (width && height && !fs.existsSync(dir + '/users/' + user.id + folderResized)) {
      let classUpload = new Upload(req, res, app);
      classUpload.createPath(dir + '/users/' + user.id + folderResized);
      var optionsObj = [dir + '/users/' + user.id + '/images/' + name, '-resize', width + 'x' + height, dir + '/users/' + user.id + folderResized + '/' + width + 'x' + height + '_' + name];

      im.convert(optionsObj, function (err, stdout) {
        if (err) throw err;
        var img = fs.readFileSync(dir + '/users/' + user.id + folderResized + '/' + (width && height ? width + 'x' + height + '_' : '') + name);
        var encode_image = img.toString('base64');

        let image = new Buffer.alloc(8, encode_image, encoding);
        var arr = (new Uint8Array(image)).subarray(0, 4);
        var header = "";
        for (var i = 0; i < arr.length; i++) {
          header += arr[i].toString(16);
        }
        console.log('header ---->', header);

        let imageType = mimeType(header);
        if (!imageType) {
          throw new Error('Ooops! We have a broken image! Contact support for image ' + (width && height ? width + 'x' + height + '_' : '') + name + ' for user ' + user_id + '!');
        }

        res.setHeader('Content-Type', imageType);
        fs.createReadStream(path.join(dir + '/users/' + user.id + folderResized, (width && height ? width + 'x' + height + '_' : '') + name)).pipe(res);
      });
    } else {

      var img = fs.readFileSync(dir + '/users/' + user.id + folderResized + '/' + (width && height ? width + 'x' + height + '_' : '') + name);
      var encode_image = img.toString('base64');

      let image = new Buffer.alloc(8, encode_image, encoding);
      var arr = (new Uint8Array(image)).subarray(0, 4);
      var header = "";
      for (var i = 0; i < arr.length; i++) {
        header += arr[i].toString(16);
      }
      console.log('header ---->', header);

      let imageType = mimeType(header);
      if (!imageType) {
        throw new Error('Ooops! We have a broken image! Contact support for image ' + (width && height ? width + 'x' + height + '_' : '') + name + ' for user ' + user_id + '!');
      }

      res.setHeader('Content-Type', imageType);
      fs.createReadStream(path.join(dir + '/users/' + user.id + folderResized, (width && height ? width + 'x' + height + '_' : '') + name)).pipe(res);
    }
  });

  // Get our initialized service so that we can register hooks
  const service = app.service('users');

  service.hooks(hooks);
  service.publish((data, hook) => {
    return app.channel(`connections/${data.to}`)
  })
};
