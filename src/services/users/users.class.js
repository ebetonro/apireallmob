const { Service } = require('feathers-sequelize');
const Sequelize = require('sequelize');

exports.Users = class Users extends Service {
  create(data, params) {
    if (!data.email_changed) {
      data.email_changed = 0
    }
    if (!data.email_validated) {
      data.email_validated = 0
    }
    if (!data.username) {
      data.username = data.name
    }
    // });
    // console.log('data', data);

    // let checkUser = Service._find({
    //   attributes: ['email'],
    //   where: {
    //     email: data.email
    //   }
    // console.log('checkUser:', checkUser);
    return super.create(data, params);
  }
  find(params) {
    // console.log('find', params)
    return super.find(params);
  }
  get(id, params) {
    // for ( let includeModel in params.sequelize.include) {
    //   params.sequelize.include[includeModel].where = id ? { user_id: id } : undefined;
    // }
    // console.log('user.class: ', params.sequelize.include);
    // console.log('user.class: ', params.sequelize.include[includeModel])
    return super.get(id, params);
  }

  remove(id, params) { return super.remove(id, params) }

  update(id, data, params) {
    if (!data.updatedBy) {
      data.updatedBy = params.user.id
    }
    return super.update(id, data, params)
  }
  patch(id, data, params) {
    if (!data.updatedBy) {
      data.updatedBy = params.user.id
    }
    return super.patch(id, data, params)
  }
};
