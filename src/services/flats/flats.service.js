// Initializes the `flats` service on path `/flats`
const { Flats } = require('./flats.class');
const createModel = require('../../models/flats.model');
const hooks = require('./flats.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app)
    // paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/flats', new Flats(options, app), async function (req, res, next) {
    const result = res.data;
    if ((req.method === 'POST' || req.method === 'post') && !result.id) {
      let newData = await app.service('flats').find({
        query: {
          name: result.name,
          createdBy: result.createdBy,
          createdAt: result.createdAt,
        }
      });
      res.data.id = newData[0].id;
    }
    next()
  });

  // Get our initialized service so that we can register hooks
  const service = app.service('flats');

  service.hooks(hooks);
};
