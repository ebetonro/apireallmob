// Initializes the `authmanagement` service on path `/authmanagement`
// const createModel = require('../../models/authmanagement.model');
// const { Authmanagement } = require('./authmanagement.class');
const authManagement = require('feathers-authentication-management');
const hooks = require('./authmanagement.hooks');
const notifier = require('./notifier');

module.exports = function (app) {
  // const options = {
  //   Model: createModel(app),
  //   paginate: app.get('paginate')
  // };

  // Initialize our service with any options it requires
  // app.use('/authmanagement', new Authmanagement(options, app));
  app.configure(authManagement(notifier(app)));

  // Get our initialized service so that we can register hooks
  const service = app.service('authManagement');
  service.hooks(hooks);
};
