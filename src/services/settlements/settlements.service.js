// Initializes the `settlements` service on path `/settlements`
const { Settlements } = require('./settlements.class');
const createModel = require('../../models/settlements.model');
const hooks = require('./settlements.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
  };

  // Initialize our service with any options it requires
  app.use('/settlements', new Settlements(options, app), async function(req, res, next) {
    const result = res.data;
    const data = result.data; // will be either `result` as an array or `data` if it is paginated
    console.log(data); // when `data` will be used, this should disappear
    next();
  });

  app.use('/settlements/:id', new Settlements(options, app), async function(req, res, next) {
    const result = res.data;
    const data = result.data; // will be either `result` as an array or `data` if it is paginated
    console.log(data); // when `data` will be used, this should disappear
    next();
  });

  // Get our initialized service so that we can register hooks
  const service = app.service('settlements');

  service.hooks(hooks);
};
