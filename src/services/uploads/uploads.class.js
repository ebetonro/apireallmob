var path = require('path');
// var fs = require('fs');

let appDir = path.dirname(require.main.filename);
let dir = appDir + '/uploads';

/* eslint-disable no-unused-vars */
exports.Uploads = class Uploads {
  constructor (options) {
    this.options = options || {};
  }

  async find (params) {
    return [];
  }

  async get (id, params) {
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  async create (data, params) {
    let { user } = params;
    // console.log('data class: ---<>', data, params);
    // console.log('this.app ==== ', params);
    if (user && user.id) {
      dir = dir + '/users';
      // if (!fs.existsSync(dir)){
      //   fs.mkdirSync(dir);
      // }
      // dir = dir + '/' + user.id;
      // if (!fs.existsSync(dir + '/' + user.id)){
      //   fs.mkdirSync(dir);
      // }
    }

    if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }

    return data;
  }

  async update (id, data, params) {
    return data;
  }

  async patch (id, data, params) {
    return data;
  }

  async remove (id, params) {
    return { id };
  }
};
