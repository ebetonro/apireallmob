const { Service } = require('feathers-sequelize');

exports.FlatsTypes = class FlatsTypes extends Service {
  get(id, params) { return super.get(id, params) }
  remove(id, params) { return super.remove(id, params) }
  create(data, params) {
    if (!data.createdBy) {
      data.createdBy = params.user.id;
    }
    return super.create(data, params);
  }

  update(id, data, params) { return super.update(id, data, params) }
  patch(id, data, params) { return super.patch(id, data, params) }

  find(params) { return super.find(params) }
};
