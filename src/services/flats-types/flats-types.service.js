// Initializes the `flats_types` service on path `/flats_types`
const { FlatsTypes } = require('./flats-types.class');
const createModel = require('../../models/flats-types.model');
const hooks = require('./flats-types.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: false // app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/flats-types', new FlatsTypes(options, app), async function(req, res, next) {
    const result = res.data;
    const data = result.data; // will be either `result` as an array or `data` if it is paginated
    console.log(data); // when `data` will be used, this should disapear
    next();
  });
  //
  // app.use('/flats-types/:id', new FlatsTypes(options, app), async function(req, res, next) {
  //   const result = res.data;
  //   const data = result.data; // will be either `result` as an array or `data` if it is paginated
  //   next();
  // });

  // Get our initialized service so that we can register hooks
  const service = app.service('flats-types');

  service.hooks(hooks);
  // service.publish((data, hook) => {
  //   return app.channel(`connections/${data.to}`)
  // })
};
