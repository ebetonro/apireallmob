// Initializes the `flats_sales` service on path `/flats-sales`
const { FlatsSales } = require('./flats-sales.class');
const createModel = require('../../models/flats-sales.model');
const hooks = require('./flats-sales.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/flats-sales', new FlatsSales(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('flats-sales');

  service.hooks(hooks);
};
