const { Service } = require('feathers-sequelize');

exports.FlatsSales = class FlatsSales extends Service {
  get(id, params) { return super.get(id, params) }
  remove(id, params) { return super.remove(id, params) }
  create(data, params) { return super.create(data, params) }

  update(id, data, params) { return super.update(id, data, params) }
  patch(id, data, params) { return super.patch(id, data, params) }

  find(params) { return super.find(params) }
};
