const { Service } = require('feathers-sequelize');

exports.documentsTypes = class documentsTypes extends Service {
  find(params) {
    return super.find(params);
  }
  get(id, params) { return super.get(id, params); }
  remove(id, params) { return super.remove(id, params); }

  create(data, params) {
    if (!data.createdBy) {
      data.createdBy = params.user.id;
    }
    return super.create(data, params);
  }

  update(id, data, params) {
    return super.update(id, data, params);
  }
  patch(id, data, params) {
    if (!data.updatedBy && !data.deletedAt) {
      data.updatedBy = params.user.id;
    } else if (!data.updatedBy && data.deletedAt) {
      data.deletedBy = params.user.id;
    }
    return super.patch(id, data, params);
  }
};
