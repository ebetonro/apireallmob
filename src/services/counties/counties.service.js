// Initializes the `counties` service on path `/counties`
const { Counties } = require('./counties.class');
const createModel = require('../../models/counties.model');
const hooks = require('./counties.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
  };

  // Initialize our service with any options it requires
  app.use('/counties', new Counties(options, app), async function(req, res, next) {
    const result = res.data;
    const data = result.data; // will be either `result` as an array or `data` if it is paginated
    console.log(data); // when `data` will be used, this should disappear
    next();
  });

  app.use('/counties/:id', new Counties(options, app), async function(req, res, next) {
    const result = res.data;
    const data = result.data; // will be either `result` as an array or `data` if it is paginated
    console.log(data); // when `data` will be used, this should disappear
    next();
  });

  // Get our initialized service so that we can register hooks
  const service = app.service('counties');

  service.hooks(hooks);
};
