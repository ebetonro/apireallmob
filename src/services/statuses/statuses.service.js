// Initializes the `statuses service on path `/statuses`
const { statuses } = require('./statuses.class');
const createModel = require('../../models/statuses.model');
const hooks = require('./statuses.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    // paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/statuses', new statuses(options, app));

  // app.use('/statuses/:id', new statuses(options, app), async function(req, res, next) {
  //   const result = res.data;
  //   const data = result.data; // will be either `result` as an array or `data` if it is paginated
  //   console.log(data); // when `data` will be used, this should disapear
  //   next();
  // });

  // Get our initialized service so that we can register hooks
  const service = app.service('statuses');

  service.hooks(hooks);
};
