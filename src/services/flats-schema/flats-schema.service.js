// Initializes the `flats_schema` service on path `/flats-schema`
const { FlatsSchema } = require('./flats-schema.class');
const createModel = require('../../models/flats-schema.model');
const hooks = require('./flats-schema.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/flats-schema', new FlatsSchema(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('flats-schema');

  service.hooks(hooks);
};
