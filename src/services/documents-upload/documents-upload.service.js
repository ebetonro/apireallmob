// Initializes the `documents-upload` service on path `/documents-upload`
const { DocumentsUpload } = require('./documents-upload.class');
const hooks = require('./documents-upload.hooks');

const multer = require('multer');

var path = require('path');
var fs = require('fs');

let appDir = path.dirname(require.main.filename);
let dir = appDir + '/documentsUpload';

// check if exists dir uploads
// if is not create it
if (!fs.existsSync(dir)){
  fs.mkdirSync(dir);
}

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, dir);
  },
  filename: function (req, file, cb) {
    cb(null, `${file.originalname}`);
  }
});
const upload = multer({
  storage,
  limits: {
    fieldSize: 1e+8, // Max field value size in bytes, here it's 100MB
    fileSize: 1e+7 //  The max file size in bytes, here it's 10MB
    // files: the number of files
    // READ MORE https://www.npmjs.com/package/multer#limits
  }
});

module.exports = function (app) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/documents-upload', new DocumentsUpload(options, app), upload.array('files'), async (req, res, next) => {
    const { method } = req;

    if (method === 'POST' || method === 'PATCH') {
      console.log(req.params);
      const { files } = req;

      for (var indFile of files) {
        // fileRes.push(
        await app.service('documents').create({
          id: null,
          name: indFile.originalname,
          description: 'YEEEEEE',
          document_type_id: 0,
          owner_user_id: 18,
          folder: 18 + '/documents',
          url: indFile.path,
          createdBy: 18,
          updatedBy: null,
          archivedAt: null,
          archivedBy: null
        });
        // );
      }
      // res.send(fileRes)
    }
    next();
  });

  // Get our initialized service so that we can register hooks
  const service = app.service('documents-upload');

  service.hooks(hooks);
};
