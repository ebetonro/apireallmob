// Initializes the `residential_complexes` service on path `/flats-rooms-types.model`
const { FlatsRoomsTypes } = require('./flats-rooms-types.class');
const createModel = require('../../models/flats-rooms-types.model');
const hooks = require('./flats-rooms-types.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: false// app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/flats-rooms-types', new FlatsRoomsTypes(options, app), async function(req, res, next) {
    const result = res.data;
    const data = result.data; // will be either `result` as an array or `data` if it is paginated
    console.log(data); // when `data` will be used, this should disapear
    next();
  });
  //
  // app.use('/flats-rooms-types/:id', new FlatsRoomsTypes(options, app), async function(req, res, next) {
  //   const result = res.data;
  //   const data = result.data; // will be either `result` as an array or `data` if it is paginated
  //   next();
  // });

  // Get our initialized service so that we can register hooks
  const service = app.service('flats-rooms-types');

  service.hooks(hooks);
  // service.publish((data, hook) => {
  //   return app.channel(`connections/${data.to}`)
  // })
};
