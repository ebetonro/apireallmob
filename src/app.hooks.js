// Application hooks that run for every service
// const addAssociations = require('./hooks/add-associations');
// const logger = require('./hooks/logger');
// const { when } = require('feathers-hooks-common');
// const authorize = require('./hooks/abilities');
const queryMenu = require('./hooks/authorize');
// const authenticate = require('./hooks/authenticate');

module.exports = {
  before: {
    all: [
      queryMenu(),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
