// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

module.exports = function(options = {}){
  // options.models = options.models || [];

  return async context => {
    let treeMenu = {};

    // Get the Sequelize instance. In the generated application via:
    const sequelize = context.app.get('sequelizeClient');

    const reconstructData = function (data, index, elementToBecomeObj) {
      if (data && data[index] && data[index][elementToBecomeObj]) {
        createMenu[data[index][elementToBecomeObj]] = [];

        if (data[index].role && data[index].role.permissions && data[index].role.permissions.length > 0) {
          for (let indexPermissions in data[index].role.permissions) {

            let permissionItem = data[index].role.permissions[indexPermissions];
            if (permissionItem.menu && permissionItem.menu.name) {
              createMenu[data[index][elementToBecomeObj]].push(permissionItem.menu);
              // delete data[index].role.permissions[indexPermissions].menu;
            }
          }
        }
        newObj[data[index][elementToBecomeObj]] = data[index];

        if (data[index + 1]) {
          reconstructData(data, (index + 1), elementToBecomeObj)
        }
      }

    };
    const getFields = function(input, field, value, type) {
      type = type || 'field';
      value = value || false;

      var output = [];
      if (type === 'object' && value) {
        for( let i in input)
          if (input[i][field] === value)
            output = input[i];
        return output
      } else {
        for (var i=0; i < input.length ; ++i)
          output.push(input[i][field]);
        return output;
      }
    };

    const constructMenu = function (index, elementToBecomeObj, parents ) {

      if ( parents.length > 0 && parents[index]) {

        var found = Object.keys(createMenu).filter(function(key) {
          for (let indexFor in createMenu[key] ) {
            if (parseInt(createMenu[key][indexFor].parent_id) === parseInt(parents[index].id)) {
              return createMenu[key][indexFor]
            }
          }
        });

        for (let indexComplex_id in found) {
          let newSomeChildElementMenu = {};
          // if (!treeMenu['complex_id_' + found[indexComplex_id]]) {
          //   treeMenu['complex_id_' + found[indexComplex_id]] = {};
          // }
          if (!treeMenu[found[indexComplex_id]]) {
            treeMenu[found[indexComplex_id]] = {};
          }

          for (var indexD in createMenu[found[indexComplex_id]]) {
            if (parseInt(createMenu[found[indexComplex_id]][indexD].parent_id) === parseInt(parents[index].id)) {
              newSomeChildElementMenu[createMenu[found[indexComplex_id]][indexD][elementToBecomeObj]] = createMenu[found[indexComplex_id]][indexD]
            }
          }

          treeMenu[found[indexComplex_id]][parents[index][elementToBecomeObj]] = parents[index];
          treeMenu[found[indexComplex_id]][parents[index][elementToBecomeObj]].childrens = newSomeChildElementMenu;
        }

        if (parents[index + 1]) {
          constructMenu(index + 1, 'position', parents)
        } else {
          return true
        }
      }
    };

    const sortMenuTree = function() {
      let objectMenu = createMenu;
      createMenu = {};

      function dynamicSort(property) {
        var sortOrder = 1;
        if(property[0] === "-") {
          sortOrder = -1;
          property = property.substr(1);
        }
        return function (a,b) {
          /* next line works with strings and numbers,
           * and you may want to customize it to your needs
           */
          var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
          return result * sortOrder;
        }
      }
      function dynamicSortMultiple() {
        /*
         * save the arguments object as it will be overwritten
         * note that arguments object is an array-like object
         * consisting of the names of the properties to sort by
         */
        var props = arguments;
        return function (obj1, obj2) {
          var i = 0, result = 0, numberOfProperties = props.length;
          /* try getting a different result from 0 (equal)
           * as long as we have extra properties to compare
           */
          while(result === 0 && i < numberOfProperties) {
            result = dynamicSort(props[i])(obj1, obj2);
            i++;
          }
          return result;
        }
      }

      for (let objComplexMenu in objectMenu) {
        if (objectMenu[objComplexMenu]['0']) {
          createMenu[objComplexMenu] = objectMenu[objComplexMenu].sort(dynamicSortMultiple('parent_id', 'position'))
        } else {
          createMenu[objComplexMenu] = objectMenu
        }
      }
    };

    let result = context.result;
    var newObj = {};
    var createMenu = {};

    if (!result.length) {
      if (result.isSuperAdmin === 1) {
        let userRoles_super = await sequelize.query('SELECT id AS residential_complex_id, 1 AS role_id, ' + result.id + ' AS user_id FROM `residential_complexes` ORDER BY `id`', {type: sequelize.QueryTypes.SELECT});

        let role = await sequelize.query('SELECT `id`, `name`, `description` FROM `roles` WHERE id = 1', {type: sequelize.QueryTypes.SELECT});
        let permission = await sequelize.query('SELECT `id`, `name`, `slug`, `menu_id`, `status`, `view`, `create`, `update`, `delete`, `export`, `all` FROM `permissions` GROUP BY `slug`', {type: sequelize.QueryTypes.SELECT});

        // let permsId_menu = getFields(permission, 'menu_id');
        let menu = await sequelize.query('SELECT `id`, `name`, `slug`, `description`, `parent_id`, `status`, `for_permission`, `visible`, `position`, `icon` FROM `menu`', {type: sequelize.QueryTypes.SELECT});

        for ( indexPerms in permission)
          permission[indexPerms].menu = getFields(menu, 'id', permission[indexPerms].menu_id, 'object');

        role = role[0];

        role.permissions = permission;

        for(indexURS in userRoles_super)
          userRoles_super[indexURS].role = role;

        let parents = await sequelize.query( 'SELECT *, 0 AS childrens FROM `menu` WHERE parent_id=0 AND for_permission=0 ORDER BY `position`', { type: sequelize.QueryTypes.SELECT } );

        reconstructData(userRoles_super, 0, 'residential_complex_id');
        sortMenuTree();
        constructMenu(0, 'position', parents);
        newObj.menu = treeMenu;

        result.userRoles = newObj;

      } else {
        let { userRoles } = result;

        if (!userRoles[0].dataValues) {
          reconstructData(userRoles, 0, 'residential_complex_id');
          sortMenuTree();

          let parents = await sequelize.query( 'SELECT *, 0 AS childrens FROM `menu` WHERE parent_id=0 AND status=1 AND for_permission=0 AND visible=1 ORDER BY `position`', { type: sequelize.QueryTypes.SELECT } );

          constructMenu(0, 'position', parents);
          newObj.menu = treeMenu;

          result.userRoles = newObj;
        }
      }
    }

    return context;
  }
};
