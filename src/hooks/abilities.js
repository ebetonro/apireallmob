// const { AbilityBuilder, Ability } = require('@casl/ability');
// const { rulesToQuery } = require('@casl/ability/extra');
// // const { toMongoQuery } = require('@casl/mongoose');
// const { Forbidden } = require('@feathersjs/errors');
// const TYPE_KEY = Symbol.for('type');
//
// Ability.addAlias('update', 'patch');
// Ability.addAlias('view', ['get', 'find']);
// Ability.addAlias('delete', 'remove');
// Ability.addAlias('export', 'export');
//
// function ruleToQuery(rule) {
//   if (JSON.stringify(rule.conditions).includes('"$all":')) {
//     throw new Error('Sequelize does not support "$all" operator')
//   }
//   return rule.inverted ? { $not: rule.conditions } : rule.conditions
// }
//
// function subjectName(subject) {
//   if (!subject || typeof subject === 'string') {
//     return subject
//   }
//
//   return subject[TYPE_KEY]
// }
//
// function defineAbilitiesFor(user) {
//   const { rules, can } = AbilityBuilder.extract();
//
//   // can('create',['users']);
//   // can('read', ['users']);
//   // console.log(user);
//   if (user) {
//     // can('manage', ['posts', 'comments'], { author: user._id });
//     can([ 'read'], 'users', { _id: user._id })
//   }
//
//   if (process.env.NODE_ENV !== 'production') {
//     // can('create', ['users'])
//   }
//
//   return new Ability(rules, { subjectName })
// }
//
// function canReadQuery(query) {
//   return query !== null
// }
//
// module.exports = function authorize(name = null) {
//   return async function(hook) {
//     const action = hook.method;
//     const service = name ? hook.app.service(name) : hook.service;
//     const serviceName = name || hook.path;
//     if (serviceName !== 'authentication') {
//       const ability = defineAbilitiesFor(hook.params.user);
//       const throwUnlessCan = (action, resource) => {
//         // console.log('ability - throwUnlessCan: ', action)
//         if (ability.cannot(action, resource)) {
//           throw new Forbidden(`You are not allowed to ${action} ${serviceName}`)
//         }
//       };
//
//       hook.params.ability = ability;
//
//       // console.log('abilities: ', serviceName);
//       if (hook.method === 'create') {
//         hook.data[TYPE_KEY] = serviceName;
//         throwUnlessCan('create', hook.data)
//       }
//
//       if (!hook.id) {
//         // const query = toMongoQuery(ability, serviceName, action);
//         const query = rulesToQuery(ability, action, serviceName, ruleToQuery);
//
//         if (canReadQuery(query)) {
//           Object.assign(hook.params.query, query)
//         } else {
//           // The only issue with this is that user will see total amount of records in db
//           // for the resources which he shouldn't know.
//           // Alternative solution is to assign `__nonExistingField` property to query
//           // but then feathers-mongoose will send a query to MongoDB which for sure will return empty result
//           // and may be quite slow for big datasets
//           hook.params.query.$limit = 0
//         }
//
//         return hook
//       }
//
//       const params = Object.assign({}, hook.params, {provider: null});
//       const result = await service.get(hook.id, params);
//
//       result[TYPE_KEY] = serviceName;
//       throwUnlessCan(action, result);
//
//       if (action === 'get') {
//         hook.result = result
//       }
//     }
//     return hook
//   }
// };

const { Ability, AbilityBuilder, toMongoQuery } = require('casl');
const { rulesToQuery } = require('@casl/ability/extra');
const TYPE_KEY = Symbol.for('type');

const { Forbidden } = require('feathers-errors');

const _ = require('lodash');

Ability.addAlias('update', 'patch');
Ability.addAlias('insert', 'create');
Ability.addAlias('read', ['get', 'find']);
Ability.addAlias('remove', 'delete');

function subjectName(subject) { // <--- added
  if (!subject || typeof subject === 'string') {
    return subject
  }

  return subject[TYPE_KEY]
}

function ruleToQuery(rule) {
  if (JSON.stringify(rule.conditions).includes('"$all":')) {
    throw new Error('Sequelize does not support "$all" operator')
  }
  return rule.inverted ? { $not: rule.conditions } : rule.conditions
}

function defineAbilitiesFor(user, complexId, serviceName) {
  complexId = complexId || false;
  serviceName = serviceName || false;
  const { userRoles } = user;

  let ids = [];
  let perms = {};

  const { rules, can } = AbilityBuilder.extract();

  if (user && user.isSuperAdmin === 1) {
    can(['read', 'update', 'remove'], 'all' )
    // can('manage', ['posts', 'comments'], { author: user._id });
  } else {
    // If he is register with a role per 1 complex we need to give him access to what permissions he have
    // else he remain with read public residential complexes for sale and hes own user see
    if (userRoles.length > 0) {
      for (indexUR in userRoles) {
        ids.push(userRoles[indexUR]['residential_complex_id']);
        perms[userRoles[indexUR]['residential_complex_id']] = [];
        let {role} = userRoles[indexUR];
        if (role.permissions.length > 0) {
          let { permissions } = role;
          for (indexPerm in permissions) {
            let pathStr = permissions[indexPerm].slug.split('_').slice(1).join('-');
            // pathArr.splice(0, 1);
            // let pathStr = pathArr.join('_');
            // console.log('replace str ====>' + userRoles[indexUR]['residential_complex_id'] + '===>', pathStr);
            let canDo= {
              [pathStr]: {
                read: permissions[indexPerm].view !== null ? permissions[indexPerm].update : false,
                update: permissions[indexPerm].update !== null ? permissions[indexPerm].update : false,
                create: permissions[indexPerm].create !== null ? permissions[indexPerm].create : false,
                remove: permissions[indexPerm].delete !== null ? permissions[indexPerm].update : false,
                // update: permissions[indexPerm].export !== null ? permissions[indexPerm].update : false,
                all: permissions[indexPerm].all !== null ? permissions[indexPerm].update : false,
              }
            };
            perms[userRoles[indexUR]['residential_complex_id']].push(canDo)
            // perms.push({})
          }
        }
        // console.log('role =>>> ', role);
      }
    }

    can('read', 'users', {id: user.id});
    can('read', 'residential_complexes', {id: {'$in': ids}, public: 1});
    can('read', 'buildings');
    can('read', 'flats');
    can('read', 'flats-sales');

    if (userRoles.length > 0 && serviceName !== 'users' && complexId) {
      perms[complexId].forEach(item => {
        let keyPath = (Object.keys(item).length > 0 ? Object.keys(item)[0] : false);

        console.log('abil path =>>>', keyPath);
        console.log('abil item[keyPath] =>>>', item[keyPath]);
        if (keyPath) {
          if (item[keyPath].all) {
            can(['read', 'insert', 'update', 'remove'], keyPath, {});
          } else {

            /**
             * TODO Conditions for bussines to be like conditions if he can do something, or overwrite some permissions
             */

            if (item[keyPath].read && item[keyPath].read !== 0) can('read', keyPath, {});
            if (item[keyPath].create && item[keyPath].create !== 0) can('insert', keyPath, {});
            if (item[keyPath].update && item[keyPath].update !== 0) can('update', keyPath, {});
            if (item[keyPath].remove && item[keyPath].remove !== 0) can('remove', keyPath, {});
          }
        }
        console.log('key =>>>>', keyPath);
      })
    }
  }

  return new Ability(rules, { subjectName })  // <--- passed subjectName option
}

function canReadQuery(query) {
  // console.log('aloha ====>>>', query['$or'][0]);
  return query !== null
}

module.exports = function authorize(name = null) {
  return async function(hook) {
    const action = hook.method;
    // const ability = defineAbilitiesFor(hook.params.user);
    const service = name ? hook.app.service(name) : hook.service;
    const serviceName = name || hook.path;
    console.info('.............');

    // console.log('condition=>>> ', hook.params.headers.authorization)
    if (serviceName !== 'authentication' && hook.params.headers.authorization) {
      console.log('abilities =>>> ', serviceName);
      console.log('abilities data =>>> ', hook.data);
      // Get the Sequelize instance. In the generated application via:
      const sequelize = hook.app.get('sequelizeClient');

      if(!_.get(hook, 'params.user.id')) {
        throw new errors.NotAuthenticated(`The current user is missing. You must not be authenticated.`);
      }
      const {user} = hook.params;


      if (user.isSuperAdmin === 1) {
        hook.params.ability = defineAbilitiesFor(user);
      } else {
        hook.params.ability = defineAbilitiesFor(user, hook.id, serviceName);
      }

      if (hook.data) {
        if (hook.params.ability.cannot(action, serviceName) && user.isSuperAdmin !== 1) {
          throw new Forbidden(`You are not allowed to ${action} ${serviceName}`)
        }
      }

      if (!hook.id) {
        // const query = toMongoQuery(ability, serviceName, action);
        // const rules = hook.params.ability.rulesFor(action, serviceName);

        const query = rulesToQuery(hook.params.ability, action, serviceName, ruleToQuery);

        if (canReadQuery(query)) {
          Object.assign(hook.params.query, query)
        } else {
          // The only issue with this is that user will see total amount of records in db
          // for the resources which he shouldn't know.
          // Alternative solution is to assign `__nonExistingField` property to query
          // but then feathers-mongoose will send a query to MongoDB which for sure will return empty result
          // and may be quite slow for big datasets
          hook.params.query.$limit = 0
        }

        return hook
      }
      console.log('author ==> ', hook.id);

      const params = Object.assign({}, hook.params, {provider: null});
      const result = await service.get(hook.id, params);

      result[TYPE_KEY] = serviceName;

      if (hook.params.ability.cannot(action, result)) {
        throw new Forbidden(`You are not allowed to ${action} ${serviceName}`)
      }
      // console.log('author ==> ', result)
      if (action === 'get') {
        Object.assign(hook.params.query, { deletedAt: null });
        hook.result = result
      }
    }

    return hook
  }
};

