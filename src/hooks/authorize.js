// Use this hook to manipulate outgoing data.

module.exports = (name = null) => {
  return async context => {
    const action = context.method;
    // const service = name ? context.app.service(name) : context.service;

    const serviceName = name || context.path;
    const sequelize = context.app.get('sequelizeClient');

    const { users_roles, roles, permissions, users, menu, documents } = sequelize.models;
    console.log('serviceName =>>>', serviceName);
    console.log('action =>>>', action);
    if (context.params /*&& context.params.provider */&& serviceName === 'users') {
    // if (context.params && serviceName === 'users') {
      // const ability = defineAbilitiesFor(hook.params.user);
      // console.log('hook =>>>', context.type);
      // console.log('context.params.provider =>>>', context.params.provider);
      // const { user } = context.params;
      //
      // console.log('context.params.user =>>>', user);

      context.params.sequelize = {
        include: [
          { model: users_roles, attributes: ['user_id', 'role_id', 'residential_complex_id'], as: 'userRoles', where: { status: 1 }, include: [
            { model: roles, attributes: ['id', 'name', 'description'], as: 'role', include: [
              { model: permissions, attributes: ['id', 'name', 'slug', 'menu_id', 'status', 'view', 'create', 'update', 'delete', 'export', 'all'], as: 'permissions', include: [
                { model: menu, attributes: ['id', 'name', 'slug', 'description', 'parent_id', 'status', 'for_permission', 'visible', 'position', 'icon'], as: 'menu', where: { status: 1 }}
              ]},
            ]},
          ]},
          {
            model: documents, attributes: ['id', 'name', 'description'], as: 'avatar'
          }
        ],
        raw: false,
      };
    }
    // console.log('-->', context.path/* context.params.query */);
    return context;
  };
};
