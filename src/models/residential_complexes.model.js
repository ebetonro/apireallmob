// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const residentialComplexes = sequelizeClient.define('residential_complexes', {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      length: 11,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true,
      unique: false,
    },
    company_id: {
      type: DataTypes.INTEGER,
      length: 100,
      unique: false,
    },
    owner_contact_id: {
      type: DataTypes.INTEGER,
      length: 100,
      unique: false,
    },
    archived: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: true,
      default: 0,
    },
    public: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: true,
      default: 0,
    },
    address_body: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
    },
    settlement_id: {
      type: DataTypes.INTEGER,
      length: 100,
      unique: false
    },
    postal_code: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true,
    },
    phone: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true,
    },
    email: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true,
    },
    contact_id: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    language: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true,
    },
    country: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true,
    },
    documents: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      unique: false,
      default: 0
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
      unique: false,
      default: 0,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  residentialComplexes.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    //const { buildings, flats } = models;
    const { buildings } = models;

    residentialComplexes.hasMany(buildings, { as: 'residential_buildings', foreignKey: 'residential_complex_id', sourceKey: 'id' });
    //residentialComplexes.belongsToMany(flats, { as: 'flats', foreignKey: 'block_id', through: { model: 'buildings', foreignKey: 'id' } })
  };

  return residentialComplexes;
};
