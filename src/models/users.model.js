// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const users = sequelizeClient.define('users', {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      length: 100,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true,
      unique: false,
    },
    email: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: false,
      unique: true,
    },
    phone: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true,
      unique: false,
    },
    contact_id: {
      type: DataTypes.INTEGER,
      length: 100,
      allowNull: true,
      unique: false,
    },
    title: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: false,
      unique: true,
    },
    job_title: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: false,
      unique: true,
    },
    username: {
      type: DataTypes.STRING,
      length: 30,
      allowNull: true,
      unique: true,
    },
    password: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: false,
    },
    language: {
      type: DataTypes.CHAR,
      length: 3,
      allowNull: true,
      default: null,
    },
    status: {
      type: DataTypes.STRING,
      length: 1,
      allowNull: false,
      default: 1,
      unique: false,
    },
    access_until: {
      type: DataTypes.DATE,
      length: 100,
      allowNull: true,
      default: null,
      unique: false,
    },
    tempAccessStartDate: {
      type: DataTypes.DATE,
      default: null,
      allowNull: true,
      unique: false
    },
    terms_accepted: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: false,
    },
    email_validated: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: false,
      default: 0,
    },
    email_changed: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: false,
      default: 0,
    },
    concent_accepted: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: false,
      default: 0,
    },
    isVerified: {
      type: DataTypes.BOOLEAN,
      default: false,
    },
    verifyToken: { type: DataTypes.STRING, default: '' },
    verifyExpires: { type: DataTypes.DATE, default: 0 },
    verifyChanges: {
      type: DataTypes.TEXT,
      get: function () {
        return this.getDataValue('verifyChanges') !== null && typeof this.getDataValue('verifyChanges') === 'object' ? JSON.parse(this.getDataValue('verifyChanges')) : {};
      },
      set: function (value) {
        this.setDataValue('verifyChanges', value !== null && typeof value === 'object' ? JSON.stringify(value) : JSON.stringify({}));
      },
      default: {},
    },// JsonField(sequelizeClient, 'users', 'verifyChanges'),
    resetToken: { type: DataTypes.STRING, default: '' },
    resetExpires: { type: DataTypes.DATE, default: new Date() },


    auth0Id: { type: Sequelize.STRING },
    isSuperAdmin: {
      type: DataTypes.TINYINT,
      length: 1,
      unique: false,
    },
    googleId: { type: Sequelize.STRING },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      unique: false,
      default: 0
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
      unique: false,
      default: 0,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  users.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    // users.hasMany(models.users_roles, {
    //   as: 'UserRoles',
    //   foreignKey: 'user_id',
    //   targetKey: 'id'
    // });
    const { users_roles, roles, permissions, menu, documents } = models;
    users.hasMany(users_roles, {
      foreignKey: 'user_id',
      sourceKey: 'id',
      as: 'userRoles'
    });
    users.hasOne(roles, {
      foreignKey: 'id',
      through: {
        model: 'users_roles',
        foreignKey: 'role_id'
      },
      as: 'role'
    });
    users.hasOne(permissions, {
      as: 'permissions',
      foreignKey: 'role_id',
      through: {
        model: 'roles',
        foreignKey: 'id'
      }
    }); // Will add role_id to comments model
    users.hasOne(menu, {
      foreignKey: 'id',
      through: {
        model: 'permissions',
        foreignKey: 'menu_id'
      },
      as: 'menu'
    }); // Will add role_id to comments model
    users.hasOne(documents, {
      foreignKey: 'owner_user_id',
      sourceKey: 'id',
      as: 'avatar'
    }); // Will add role_id to comments model
  };

  return users;
};
