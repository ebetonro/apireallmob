// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const menu = sequelizeClient.define('menu', {

    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      length: 11,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: false,
      unique: false,
    },
    slug: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: false,
      unique: false,
    },
    description: {
      type: DataTypes.TEXT,
    },
    parent_id: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    status: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    for_permission: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    visible: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    new_feature: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    position: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    icon: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: false,
      unique: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      unique: false,
      default: 0
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
      unique: false,
      default: 0,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  menu.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    const { users_roles, roles, permissions, menu } = models;
    // menu.belongsTo(permissions, {foreignKey: 'menu_id', sourceKey: 'id', as: 'permissions'});
    menu.hasMany(permissions, {sourceKey: 'id', foreignKey: 'menu_id', as: 'permissions'})
  };

  return menu;
};
