// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const userRoles = sequelizeClient.define('users_roles', {

    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      length: 100,
      unique: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      // references: {
      //   model: 'users',
      //   key: 'id'
      // },
      length: 11,
      allowNull: false,
      unique: false,
    },
    role_id: {
      type: DataTypes.INTEGER,
      length: 11,
      // references: {
      //   model: 'roles',
      //   key: 'id'
      // },
      allowNull: false,
      unique: false,
    },
    residential_complex_id: {
      type: DataTypes.INTEGER,
      length: 100,
      allowNull: false,
      unique: false,
    },
    status: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: true,
      unique: false,
      default: 0,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      unique: false,
      default: 0
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
      unique: false,
      default: 0,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  userRoles.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    // eslint-disable-next-line no-unused-vars
    // const { roles, permissions, users } = models;
    const { roles, permissions } = models;
    userRoles.hasOne(roles, {foreignKey: 'id', sourceKey: 'role_id', as: 'role'});
    // userRoles.hasMany(permissions, {foreignKey: 'role_id', sourceKey: 'role_id', as: 'permissions'});
  };

  return userRoles;
};
