// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const roles = sequelizeClient.define('roles', {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      length: 11,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: false,
      unique: false,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true,
      unique: false,
    },
    is_superadmin: {
      type: DataTypes.INTEGER,
      length: 100,
      unique: true,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      unique: false,
      default: 0
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
      unique: false,
      default: 0,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  roles.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    // roles.hasMany(models.permissions, {
    //   as: 'Permissions',
    //   foreignKey: 'id',
    //   targetKey: 'role_id',
    // });
    const { permissions, users_roles } = models;
    // const { permissions, users_roles, users } = models;
    // roles.belongsTo(users, {underscored: true, foreignKey: 'id', targetKey: 'role_id', constraints: false, through: { model: users_roles}}); // Will add role_id to comments model
    roles.hasMany(permissions, {foreignKey: 'role_id', sourceKey: 'id', as: 'permissions'}); // Will add role_id to comments model
    // roles.hasOne(users, {foreignKey: 'user_id', sourceKey: 'id', as: 'users'}); // Will add role_id to comments model
    roles.hasOne(users_roles, {foreignKey: 'user_role_id', sourceKey: 'id', as: 'users_roles'}); // Will add role_id to comments model
  };

  return roles;
};
