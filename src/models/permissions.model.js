// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const permissions = sequelizeClient.define('permissions', {

    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      length: 11,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true,
      unique: false,
    },
    role_id: {
      type: DataTypes.INTEGER,
      length: 100,
      allowNull: false,
      unique: false,
    },
    slug: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true,
      unique: false,
    },
    menu_id: {
      type: DataTypes.INTEGER,
      length: 100,
      allowNull: true,
      unique: false,
    },
    status: {
      type: DataTypes.INTEGER,
      length: 100,
      allowNull: true,
      unique: false,
    },
    view: {
      type: DataTypes.INTEGER,
      length: 100,
      allowNull: true,
      unique: false,
    },
    create: {
      type: DataTypes.INTEGER,
      length: 100,
      allowNull: true,
      unique: false,
    },
    update: {
      type: DataTypes.INTEGER,
      length: 100,
      allowNull: true,
      unique: false,
    },
    delete: {
      type: DataTypes.INTEGER,
      length: 100,
      allowNull: true,
      unique: false,
    },
    export: {
      type: DataTypes.INTEGER,
      length: 100,
      allowNull: true,
      unique: false,
    },
    all: {
      type: DataTypes.INTEGER,
      length: 100,
      allowNull: true,
      unique: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      unique: false,
      default: 0
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
      unique: false,
      default: 0,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  permissions.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    // const { users_roles, users, menu } = models;
    const { menu } = models;
    permissions.hasOne(menu, {sourceKey:'menu_id', foreignKey: 'id', as: 'menu'}); // Will add menu_id to comments model
  };

  return permissions;
};
