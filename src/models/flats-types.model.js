// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const flatsTypes = sequelizeClient.define('flats_types', {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      length: 11,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
      unique: true,
    },
    residential_complex_id: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    building_id: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    util_sqm: {
      type: DataTypes.FLOAT,
      length: 11,
      unique: false,
    },
    total_sqm: {
      type: DataTypes.FLOAT,
      length: 11,
      unique: false,
    },
    no_rooms: {
      type: DataTypes.TINYINT,
      length: 2,
      unique: false,
    },
    no_baths: {
      type: DataTypes.TINYINT,
      length: 2,
      unique: false,
    },
    no_bedrooms: {
      type: DataTypes.TINYINT,
      length: 2,
      unique: false,
    },
    no_teraces: {
      type: DataTypes.TINYINT,
      length: 2,
      unique: false,
    },
    image_schema_thumb: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
      unique: false,
    },
    image_schema: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
      unique: false,
    },
    price_net: {
      type: DataTypes.FLOAT,
      length:11,
      unique: false,
    },
    vat: {
      primaryKey: true,
      type: DataTypes.TINYINT,
      length: 2,
      unique: false,
    },
    currency: {
      primaryKey: true,
      type: DataTypes.STRING,
      length: 3,
      unique: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      unique: false,
      default: 0
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
      unique: false,
      default: 0,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    }
  },
  {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  flatsTypes.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    // const { flats, flats_sales } = models;
    //
    // flatsTypes.hasMany(buildings, { foreignKey: '_id', sourceKey: 'id', as: 'building_flats' });
    // flatsTypes.belongsToMany(buildings, { foreignKey: 'statusc_id', as: 'buildings', through: { model: 'buildings', foreignKey: 'id' } });
    // flatsTypes.belongsToMany(buildings, { foreignKey: 'statusv_id', as: 'buildings', through: { model: 'buildings', foreignKey: 'id' } });
  };

  return flatsTypes;
};
