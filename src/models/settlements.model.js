// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const settlements = sequelizeClient.define('settlements', {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      length: 100,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true,
      unique: false,
    },
    type: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true,
      unique: false,
    },
    country_id: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
    },
    county_id: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      length: 11,
      unique: false,
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
      unique: false,
      default: 0,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  settlements.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    // users.hasMany(models.users_roles, {
    //   as: 'UserRoles',
    //   foreignKey: 'user_id',
    //   targetKey: 'id'
    // });
    // const { users_roles, roles, permissions, menu } = models;
    // settlements.hasMany(users_roles, {
    //   foreignKey: 'settlement_id',
    //   sourceKey: 'id',
    //   as: 'userRoles'
    // });
  };

  return settlements;
};
