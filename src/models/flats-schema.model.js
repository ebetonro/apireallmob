// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const flatsSchema = sequelizeClient.define('flats-schema', {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      length: 11,
      unique: true,
    },
    flat_rooms_type_id: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
    },
    room_sqm: {
      type: DataTypes.FLOAT,
      length: 11,
      allowNull: true,
      unique: false,
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  flatsSchema.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return flatsSchema;
};
