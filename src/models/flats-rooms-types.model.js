// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const flatsRoomsTypes = sequelizeClient.define('flats-rooms-types', {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      length: 11,
      unique: true
    },
    name: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
      unique: false
    },
    adds_in_util: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: true,
      default: 1
    },
    active: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: true,
      default: 1
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  flatsRoomsTypes.associate = function (models) { // eslint-disable-line no-unused-vars
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return flatsRoomsTypes;
};
