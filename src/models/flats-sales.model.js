// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const flatsSales = sequelizeClient.define('flats-sales', {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      length: 100,
      unique: true,
    },
    flat_id: {
      type: DataTypes.INTEGER,
      length: 11,
    },
    customer_id: {
      type: DataTypes.INTEGER,
      length: 11,
    },
    sale_status_id: {
      type: DataTypes.INTEGER,
      length: 11,
    },
    trade_price: {
      type: DataTypes.FLOAT,
    },
    initial_price: {
      type: DataTypes.FLOAT,
    },
    vat: {
      type: DataTypes.INTEGER,
      length: 11,
    },
    currency: {
      type: DataTypes.TINYINT,
      length: 1,
    },
    bank_loan: {
      type: DataTypes.TINYINT,
      length: 1,
    },
    bank_id: {
      type: DataTypes.INTEGER,
      length: 11,
    },
    agent_sign_id: {
      type: DataTypes.INTEGER,
      length: 11,
    },
    contract_no: {
      type: DataTypes.STRING,
      length: 100,
    },
    documents: {
      type: DataTypes.TEXT,
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
      unique: false,
      default: 0,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  flatsSales.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return flatsSales;
};
