// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const contacts = sequelizeClient.define('contacts', {

    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      length: 11,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
      unique: false,
    },
    gender: {
      type: DataTypes.STRING,
      length: 1,
      unique: false,
    },
    title: {
      type: DataTypes.STRING,
      length: 10,
      allowNull: false,
    },
    job_title: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: false,
    },
    phone01: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: false,
    },
    phone02: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: false,
    },
    fax: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: false,
    },
    language: {
      type: DataTypes.STRING,
      length: 2,
      allowNull: true,
    },
    country: {
      type: DataTypes.STRING,
      length: 3,
      allowNull: true,
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
      unique: false,
      default: 0,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  contacts.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    // const { contacts, flats_sales } = models;
    //
    // contacts.hasMany(flats, { foreignKey: 'block_id', sourceKey: 'id', as: 'building_flats' });
    // contacts.belongsToMany(flats_sales, { foreignKey: 'flat_id', as: 'flats_sales', through: { model: 'flats', foreignKey: 'id' } });

  };

  return contacts;
};
